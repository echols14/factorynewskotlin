package ss.virovitica.factorynewskotlin

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import io.realm.Realm
import ss.virovitica.factorynewskotlin.di.AppComponent
import ss.virovitica.factorynewskotlin.di.AppModule
import ss.virovitica.factorynewskotlin.di.DaggerAppComponent
import javax.inject.Inject

class NewsApp: Application(), HasActivityInjector, HasSupportFragmentInjector {
    companion object {
        lateinit var mAppComponent: AppComponent
            private set
    }

    @Inject lateinit var dispatchingAndroidActivityInjector: DispatchingAndroidInjector<Activity>
    @Inject lateinit var dispatchingAndroidFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidActivityInjector
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidFragmentInjector

    override fun onCreate() {
        super.onCreate()
        mAppComponent = buildComponent()
        mAppComponent.inject(this)
        Realm.init(this)
    }

    private fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}