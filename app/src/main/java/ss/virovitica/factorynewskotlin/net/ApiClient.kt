package ss.virovitica.factorynewskotlin.net

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import ss.virovitica.factorynewskotlin.model.ArticlesResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiClient @Inject constructor(private val retrofit: Retrofit) {
    companion object {
        const val TIMEOUT_SECONDS: Long = 10
        const val BASE_URL = "https://newsapi.org/"
        private const val PATH = "v1/articles"

        private const val SOURCE = "source"
        private const val SORT_BY = "sortBy"
        private const val API_KEY = "apiKey"

        private const val SOURCE_VAL = "bbc-news"
        private const val SORT_BY_VAL = "top"
        private const val API_KEY_VAL = "6946d0c07a1c4555a4186bfcade76398"
    }

    fun getAllArticles(): Observable<ArticlesResponse> {
        //build a client using retrofit, then get an Observable object from it
        return retrofit.create(NewsClient::class.java).getArticlesObservable(SOURCE_VAL, SORT_BY_VAL, API_KEY_VAL)
    }

    private interface NewsClient {
        @GET(PATH)
        fun getArticlesObservable(@Query(SOURCE) source: String, @Query(SORT_BY) sortBy: String, @Query(API_KEY) apiKey: String): Observable<ArticlesResponse>
    }
}