package ss.virovitica.factorynewskotlin.activity.pager.view

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.MenuItem
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_pager.*
import ss.virovitica.factorynewskotlin.R
import ss.virovitica.factorynewskotlin.activity.main.view.MainActivity
import ss.virovitica.factorynewskotlin.activity.pager.presenter.IPagerPresenter
import ss.virovitica.factorynewskotlin.adapter.ArticlePagerAdapter
import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.utility.ErrorDialogFragment
import ss.virovitica.factorynewskotlin.utility.ErrorDialogFragment.Companion.ERROR_DIALOG
import javax.inject.Inject

class PagerActivity : AppCompatActivity(), IPagerView {
    companion object {
        private const val EXTRA_ARTICLE_FOCUS_INDEX = "ss.virovitica.factorynewskotlin.activity.pager.view.PagerActivity.EXTRA_ARTICLE_FOCUS_INDEX"
        private const val KEY_CURRENT_ARTICLE_INDEX = "ss.virovitica.factorynewskotlin.activity.pager.view.PagerActivity.KEY_CURRENT_ARTICLE_INDEX"
        fun newIntent(context: Context, articleIndex: Int): Intent {
            val intent = Intent(context, PagerActivity::class.java)
            intent.putExtra(EXTRA_ARTICLE_FOCUS_INDEX, articleIndex)
            return intent
        }
    }

    override val context: Context
        get() = this
    private lateinit var presenter: IPagerPresenter
    private lateinit var adapter: ArticlePagerAdapter
    private var startPageIndex = -1
    private var isNew: Boolean = false
    private var isThisVisible: Boolean = false
    private var progressDialog: ProgressDialog? = null
    private var dialogIsShown: Boolean = false

    @Inject
    fun inject(presenterIn: IPagerPresenter) {
        presenter = presenterIn
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pager)
        //set up the toolbar
        setSupportActionBar(mainToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //set up the pager
        articlePager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(i: Int) {}
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) = presenter.setTitle(i)
        })
        //set up the adapter
        adapter = ArticlePagerAdapter(supportFragmentManager)
        articlePager.adapter = adapter
        //read the info from the intent of what article we want to show first
        val intent = intent
        if (intent != null) {
            startPageIndex = intent.getIntExtra(EXTRA_ARTICLE_FOCUS_INDEX, 0)
            intent.removeExtra(EXTRA_ARTICLE_FOCUS_INDEX)
        }
        isNew = true
        presenter.fetchArticles()
    }

    override fun onResume() {
        super.onResume()
        isThisVisible = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            startActivity(MainActivity.newIntent(this))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        isThisVisible = false
        super.onPause()
    }

    override fun onDestroy() {
        presenter.finish()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        val currentArticleIndex = articlePager.currentItem
        outState.putInt(KEY_CURRENT_ARTICLE_INDEX, currentArticleIndex)
        super.onSaveInstanceState(outState)
    }

    override fun setArticles(articles: Array<Article>) {
        adapter.articles = articles
        adapter.notifyDataSetChanged()
        if (isNew) { //if the article was newly created from a click in the MainActivity, select the article clicked
            articlePager.currentItem = startPageIndex
            isNew = false
        }
    }

    override fun setTitle(title: String?) {
        supportActionBar?.title = title
    }

    override fun showErrorDialog() {
        val errorDialog = ErrorDialogFragment()
        errorDialog.show(supportFragmentManager, ERROR_DIALOG)
    }

    override fun showLoadingDialog() {
        if (isThisVisible) {
            progressDialog = ProgressDialog(this)
            progressDialog?.setMessage(getString(R.string.loading_message))
            progressDialog?.isIndeterminate = false
            progressDialog?.setCancelable(false)
            progressDialog?.show()
            dialogIsShown = true
        }
    }

    override fun hideLoadingDialog() {
        if (dialogIsShown) {
            progressDialog?.hide()
            progressDialog = null
        }
    }
}
