package ss.virovitica.factorynewskotlin.activity.main.view

import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.utility.IView

interface IMainView: IView {
    fun setArticles(articles: Array<Article>)
    fun showErrorDialog()
    fun showLoadingDialog()
    fun hideLoadingDialog()
}