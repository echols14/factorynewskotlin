package ss.virovitica.factorynewskotlin.activity.main.presenter

import ss.virovitica.factorynewskotlin.interactor.IArticleInteractor
import ss.virovitica.factorynewskotlin.activity.main.view.IMainView
import ss.virovitica.factorynewskotlin.model.ArticlesResponse
import ss.virovitica.factorynewskotlin.utility.Listener
import ss.virovitica.factorynewskotlin.utility.Refresher
import javax.inject.Inject

class MainPresenter @Inject constructor(private val view: IMainView, private val interactor: IArticleInteractor,
                                        private val refresher: Refresher): IMainPresenter, Listener<ArticlesResponse> {
    init {refresher.mainPresenter = this}

    override fun startRefresh() = refresher.start()
    override fun fetchArticles() = interactor.fetchArticlesResponse(this as Listener<ArticlesResponse>)
    override fun unsubscribe() {
        interactor.unsubscribe()
        refresher.mainPresenter = null
    }
    override fun showLoadingDialog() = view.showLoadingDialog()
    override fun hideLoadingDialog() = view.hideLoadingDialog()

    //***Listener***//
    override fun onSuccess(result: ArticlesResponse) = view.setArticles(result.getArticles())
    override fun onError() = view.showErrorDialog()
}