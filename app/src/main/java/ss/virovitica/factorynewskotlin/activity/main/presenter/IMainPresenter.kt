package ss.virovitica.factorynewskotlin.activity.main.presenter

import ss.virovitica.factorynewskotlin.utility.IPresenter

interface IMainPresenter: IPresenter {
    fun fetchArticles()
    fun showLoadingDialog()
    fun hideLoadingDialog()
    fun startRefresh()
    fun unsubscribe()
}