package ss.virovitica.factorynewskotlin.activity.main.view

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import ss.virovitica.factorynewskotlin.R
import ss.virovitica.factorynewskotlin.activity.main.presenter.IMainPresenter
import ss.virovitica.factorynewskotlin.adapter.ArticleRecyclerAdapter
import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.utility.ErrorDialogFragment
import ss.virovitica.factorynewskotlin.utility.ErrorDialogFragment.Companion.ERROR_DIALOG
import javax.inject.Inject

class MainActivity : AppCompatActivity(), IMainView {
    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }
    override val context: Context
        get() = this
    private lateinit var presenter: IMainPresenter
    private lateinit var adapter: ArticleRecyclerAdapter
    private var isThisVisible: Boolean = false
    private var progressDialog: ProgressDialog? = null
    private var dialogIsShown: Boolean = false

    @Inject fun inject(presenterIn: IMainPresenter, adapterIn: ArticleRecyclerAdapter) {
        presenter = presenterIn
        adapter = adapterIn
    }

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //set up the RecyclerView
        newsRecyclerView.layoutManager = LinearLayoutManager(this)
        newsRecyclerView.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        isThisVisible = true
        presenter.startRefresh()
    }

    override fun onPause() {
        isThisVisible = false
        super.onPause()
    }

    override fun onDestroy() {
        presenter.unsubscribe() //take care of memory leaks
        super.onDestroy()
    }

    //***IMainView***//

    override fun setArticles(articles: Array<Article>) {
        adapter.articles = articles
    }

    override fun showErrorDialog() {
        val errorDialog = ErrorDialogFragment()
        errorDialog.show(supportFragmentManager, ERROR_DIALOG)
    }

    override fun showLoadingDialog() {
        if (isThisVisible) {
            progressDialog = ProgressDialog(this)
            progressDialog?.setMessage(getString(R.string.loading_message))
            progressDialog?.isIndeterminate = false
            progressDialog?.setCancelable(false)
            progressDialog?.show()
            dialogIsShown = true
        }
    }

    override fun hideLoadingDialog() {
        if (dialogIsShown) {
            progressDialog?.hide()
            progressDialog = null
        }
    }
}
