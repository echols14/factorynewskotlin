package ss.virovitica.factorynewskotlin.activity.pager.presenter

import ss.virovitica.factorynewskotlin.utility.IPresenter

interface IPagerPresenter: IPresenter {
    fun fetchArticles()
    fun setTitle(index: Int)
    fun showLoadingDialog()
    fun hideLoadingDialog()
    fun finish()
}