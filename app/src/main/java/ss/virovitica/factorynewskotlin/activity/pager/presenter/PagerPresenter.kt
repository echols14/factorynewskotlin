package ss.virovitica.factorynewskotlin.activity.pager.presenter

import ss.virovitica.factorynewskotlin.activity.pager.view.IPagerView
import ss.virovitica.factorynewskotlin.interactor.IArticleInteractor
import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.model.getArticles
import ss.virovitica.factorynewskotlin.utility.Listener
import ss.virovitica.factorynewskotlin.utility.Refresher
import javax.inject.Inject

class PagerPresenter @Inject constructor(private val view: IPagerView, private val interactor: IArticleInteractor,
                                         private val refresher: Refresher): IPagerPresenter, Listener<Array<Article>> {
    init {
        refresher.pagerPresenter = this
    }

    override fun fetchArticles() = interactor.fetchArticles(this)
    override fun setTitle(index: Int) = view.setTitle(getArticles()[index].title)
    override fun showLoadingDialog() = view.showLoadingDialog()
    override fun hideLoadingDialog() = view.hideLoadingDialog()
    override fun finish() {
        refresher.pagerPresenter = null
    }

    override fun onSuccess(result: Array<Article>) = view.setArticles(result)
    override fun onError() = view.showErrorDialog()
}