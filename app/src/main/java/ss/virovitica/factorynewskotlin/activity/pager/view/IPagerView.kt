package ss.virovitica.factorynewskotlin.activity.pager.view

import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.utility.IView

interface IPagerView: IView {
    fun setArticles(articles: Array<Article>)
    fun setTitle(title: String?)
    fun showErrorDialog()
    fun showLoadingDialog()
    fun hideLoadingDialog()
}