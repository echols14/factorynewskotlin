package ss.virovitica.factorynewskotlin.di

import javax.inject.Scope
import kotlin.annotation.Retention

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class PerActivity

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class PerFragment