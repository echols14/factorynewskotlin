package ss.virovitica.factorynewskotlin.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ss.virovitica.factorynewskotlin.fragment.ArticleFragment

@Module
abstract class FragmentModule {
    @PerFragment
    @ContributesAndroidInjector(modules = [ArticleModule::class])
    internal abstract fun contributeArticleFragment(): ArticleFragment
}