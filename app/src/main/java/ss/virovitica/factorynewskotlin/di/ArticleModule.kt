package ss.virovitica.factorynewskotlin.di

import dagger.Module

@Module
class ArticleModule {
    //no view, presenter, or interactor
    //all functions are contained in the ArticleFragment class since it's just a one-time display
}