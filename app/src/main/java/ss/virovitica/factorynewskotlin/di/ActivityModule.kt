package ss.virovitica.factorynewskotlin.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ss.virovitica.factorynewskotlin.activity.main.view.MainActivity
import ss.virovitica.factorynewskotlin.activity.pager.view.PagerActivity

@Module
abstract class ActivityModule {
    @PerActivity
    @ContributesAndroidInjector(modules = [MainModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [PagerModule::class])
    internal abstract fun contributePagerActivity(): PagerActivity
}