package ss.virovitica.factorynewskotlin.di

import dagger.Module
import dagger.Provides
import ss.virovitica.factorynewskotlin.interactor.IArticleInteractor
import ss.virovitica.factorynewskotlin.interactor.ArticleInteractor
import ss.virovitica.factorynewskotlin.activity.main.presenter.IMainPresenter
import ss.virovitica.factorynewskotlin.activity.main.presenter.MainPresenter
import ss.virovitica.factorynewskotlin.activity.main.view.IMainView
import ss.virovitica.factorynewskotlin.activity.main.view.MainActivity

@Module
class MainModule {
    @Provides @PerActivity
    fun provideView(activity: MainActivity): IMainView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: MainPresenter): IMainPresenter = presenter
    @Provides @PerActivity
    fun provideInteractor(presenter: ArticleInteractor): IArticleInteractor = presenter
}