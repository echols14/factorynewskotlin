package ss.virovitica.factorynewskotlin.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import ss.virovitica.factorynewskotlin.NewsApp
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class, AndroidInjectionModule::class, ActivityModule::class, FragmentModule::class])
interface AppComponent {
    fun inject(app: NewsApp)
}