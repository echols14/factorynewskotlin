package ss.virovitica.factorynewskotlin.di

import dagger.Module
import dagger.Provides
import ss.virovitica.factorynewskotlin.NewsApp
import javax.inject.Singleton

@Module
class AppModule(private val newsApp: NewsApp) {
    @Provides @Singleton
    internal fun provideNewsApp(): NewsApp = newsApp
    @Provides @Singleton
    internal fun provideMainModule(): MainModule = MainModule()
    @Provides @Singleton
    internal fun providePagerModule(): PagerModule = PagerModule()
}