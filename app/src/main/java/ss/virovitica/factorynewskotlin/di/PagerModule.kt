package ss.virovitica.factorynewskotlin.di

import dagger.Module
import dagger.Provides
import ss.virovitica.factorynewskotlin.activity.pager.presenter.IPagerPresenter
import ss.virovitica.factorynewskotlin.activity.pager.presenter.PagerPresenter
import ss.virovitica.factorynewskotlin.activity.pager.view.IPagerView
import ss.virovitica.factorynewskotlin.activity.pager.view.PagerActivity
import ss.virovitica.factorynewskotlin.interactor.ArticleInteractor
import ss.virovitica.factorynewskotlin.interactor.IArticleInteractor

@Module
class PagerModule {
    @Provides @PerActivity
    fun provideView(activity: PagerActivity): IPagerView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: PagerPresenter): IPagerPresenter = presenter
    @Provides @PerActivity
    fun provideInteractor(interactor: ArticleInteractor): IArticleInteractor = interactor
}