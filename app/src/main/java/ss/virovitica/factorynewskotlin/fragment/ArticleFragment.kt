package ss.virovitica.factorynewskotlin.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import dagger.android.support.AndroidSupportInjection
import org.jetbrains.anko.find
import ss.virovitica.factorynewskotlin.R
import ss.virovitica.factorynewskotlin.model.Article

class ArticleFragment: Fragment() {
    companion object {
        private const val KEY_ARTICLE = "ss.virovitica.factorynewskotlin.fragment.ArticleFragment.articleParcelKey"
        fun newInstance(article: Article): ArticleFragment {
            val newFragment = ArticleFragment()
            val bundle = Bundle()
            bundle.putParcelable(KEY_ARTICLE, article)
            newFragment.arguments = bundle
            return newFragment
        }
    }

    private lateinit var article: Article

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        val args = arguments
        if (args != null) {
            article = args.getParcelable(KEY_ARTICLE) ?: Article()
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_article, container, false)
        //grab the views
        //load the article image and text
        Glide.with(this).load(article.urlToImage).into(v.find(R.id.topImage))
        (v.find(R.id.titleText) as TextView).text = article.title
        (v.find(R.id.bodyText) as TextView).text = article.description
        return v
    }
}