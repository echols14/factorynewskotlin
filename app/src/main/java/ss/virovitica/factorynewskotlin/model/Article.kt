package ss.virovitica.factorynewskotlin.model

import android.os.Parcel
import android.os.Parcelable
import io.realm.RealmObject

open class Article(): RealmObject(), Parcelable {
    companion object {
        @JvmField
        internal val CREATOR: Parcelable.Creator<Article> = object: Parcelable.Creator<Article> { //for Parcelable
            override fun createFromParcel(parcel: Parcel): Article = Article(parcel)
            override fun newArray(size: Int): Array<Article?> = arrayOfNulls(size)
        }
    }
    var author: String? = null
        private set
    var title: String? = null
        private set
    var description: String? = null
        private set
    var url: String? = null
        private set
    var urlToImage: String? = null
        private set
    var publishedAt: String? = null
        private set

    //***Parcelable***//

    protected constructor(parcel: Parcel): this() {
        author = parcel.readString()
        title = parcel.readString()
        description = parcel.readString()
        url = parcel.readString()
        urlToImage = parcel.readString()
        publishedAt = parcel.readString()
    }
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(author)
        dest.writeString(title)
        dest.writeString(description)
        dest.writeString(url)
        dest.writeString(urlToImage)
        dest.writeString(publishedAt)
    }
}