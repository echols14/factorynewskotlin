package ss.virovitica.factorynewskotlin.model

import io.realm.Realm

fun getArticles(): Array<Article> {
    val realm = Realm.getDefaultInstance()
    val managed = realm.where(ArticlesResponse::class.java).findFirst() ?: return arrayOf()
    val unmanaged = realm.copyFromRealm(managed)
    realm.close()
    return unmanaged?.getArticles() ?: arrayOf()
}

fun saveArticles(articles: ArticlesResponse) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        it.deleteAll()
        it.copyToRealm(articles) }
    realm.close()
}