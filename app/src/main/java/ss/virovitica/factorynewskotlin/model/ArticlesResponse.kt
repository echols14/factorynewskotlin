package ss.virovitica.factorynewskotlin.model

import io.realm.RealmList
import io.realm.RealmObject

open class ArticlesResponse: RealmObject() {
    lateinit var status: String
        private set
    lateinit var source: String
        private set
    lateinit var sortBy: String
        private set
    private lateinit var articles: RealmList<Article>

    fun getArticles(): Array<Article> = articles.toTypedArray()
}