package ss.virovitica.factorynewskotlin.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import ss.virovitica.factorynewskotlin.fragment.ArticleFragment
import ss.virovitica.factorynewskotlin.model.Article

class ArticlePagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm) {
    internal var articles = arrayOf<Article>()
    override fun getItem(position: Int): Fragment = ArticleFragment.newInstance(articles[position])
    override fun getCount(): Int = articles.size
}