package ss.virovitica.factorynewskotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_preview.view.*
import ss.virovitica.factorynewskotlin.R
import ss.virovitica.factorynewskotlin.activity.pager.view.PagerActivity
import ss.virovitica.factorynewskotlin.di.PerActivity
import ss.virovitica.factorynewskotlin.model.Article
import javax.inject.Inject

@PerActivity
class ArticleRecyclerAdapter @Inject constructor(): RecyclerView.Adapter<ArticleRecyclerAdapter.ArticleHolder>() {
    var articles = arrayOf<Article>()
        set(value) {
            field = value
            this.notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, index: Int) =
        ArticleHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_preview, parent, false))

    override fun onBindViewHolder(holder: ArticleHolder, index: Int) = holder.bind(index, articles[index])

    override fun getItemCount() = articles.size

    class ArticleHolder(parent: View) : RecyclerView.ViewHolder(parent), View.OnClickListener {
        init { parent.setOnClickListener(this) }

        private lateinit var mContext: Context
        private var mIndex: Int = -1

        internal fun bind(index: Int, article: Article) {
            mIndex = index
            mContext = itemView.context
            //set image
            Glide.with(mContext).load(article.urlToImage).into(itemView.imagePreview)
            //set text field
            itemView.titleText.text = article.title
        }

        /**
         * when an Article preview is clicked, this opens an PagerActivity for that Article
         * @param view the view being clicked
         */
        override fun onClick(view: View) {
            val intent = PagerActivity.newIntent(mContext, mIndex)
            mContext.startActivity(intent)
        }
    }
}