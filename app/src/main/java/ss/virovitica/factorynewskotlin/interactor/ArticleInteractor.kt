package ss.virovitica.factorynewskotlin.interactor

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.model.ArticlesResponse
import ss.virovitica.factorynewskotlin.model.getArticles
import ss.virovitica.factorynewskotlin.model.saveArticles
import ss.virovitica.factorynewskotlin.net.ApiClient
import ss.virovitica.factorynewskotlin.utility.Listener
import ss.virovitica.factorynewskotlin.utility.Refresher
import javax.inject.Inject

class ArticleInteractor @Inject constructor(private val client: ApiClient, private val refresher: Refresher):
    IArticleInteractor {
    private var disposable: CompositeDisposable? = null

    override fun fetchArticlesResponse(listener: Listener<ArticlesResponse>) {
        disposable = CompositeDisposable()
        refresher.showLoadingDialog()
        disposable?.add(
            client.getAllArticles()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(object : DisposableObserver<ArticlesResponse>() {
                    override fun onNext(articles: ArticlesResponse) {
                        saveArticles(articles)
                        listener.onSuccess(articles)
                        refresher.hideLoadingDialog()
                    }
                    override fun onError(e: Throwable) {
                        Log.e("ArticleInteractor", e.toString())
                        listener.onError()
                    }
                    override fun onComplete() {}
                })
        )
    }

    override fun fetchArticles(listener: Listener<Array<Article>>) {
        val articles = getArticles()
        if(articles.isEmpty()) listener.onError()
        else listener.onSuccess(articles)
    }

    override fun unsubscribe() {
        if(disposable?.isDisposed == false) disposable?.dispose()
    }
}