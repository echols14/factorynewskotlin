package ss.virovitica.factorynewskotlin.interactor

import ss.virovitica.factorynewskotlin.model.Article
import ss.virovitica.factorynewskotlin.model.ArticlesResponse
import ss.virovitica.factorynewskotlin.utility.IInteractor
import ss.virovitica.factorynewskotlin.utility.Listener

interface IArticleInteractor: IInteractor {
    fun fetchArticlesResponse(listener: Listener<ArticlesResponse>)
    fun fetchArticles(listener: Listener<Array<Article>>)
    fun unsubscribe()
}