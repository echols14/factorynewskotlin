package ss.virovitica.factorynewskotlin.utility

interface Listener<T> {
    fun onSuccess(result: T)
    fun onError()
}