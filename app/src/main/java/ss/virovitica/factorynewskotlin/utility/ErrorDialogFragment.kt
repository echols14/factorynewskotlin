package ss.virovitica.factorynewskotlin.utility

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import ss.virovitica.factorynewskotlin.R

class ErrorDialogFragment: DialogFragment() {
    companion object {
        const val ERROR_DIALOG = "echols.ErrorDialog"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        //set text fields
        builder.setMessage(R.string.error_message)
            .setTitle(R.string.error)
            .setPositiveButton(R.string.ok) { _, _ ->
                //close the dialog (and go back to the main activity)
                if (activity != null) {
                    activity!!.finish()
                    System.exit(0)
                }
            }
        return builder.create()
    }
}