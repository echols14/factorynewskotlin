package ss.virovitica.factorynewskotlin.utility

import android.os.Handler
import ss.virovitica.factorynewskotlin.activity.main.presenter.IMainPresenter
import ss.virovitica.factorynewskotlin.activity.pager.presenter.IPagerPresenter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Refresher @Inject constructor() {
    companion object {
        private const val REFRESH_RATE_MS : Long = 1000*60*5
        //proučiti kako se na retrofit napravi refresh
    }

    private var isRunning = false
    var mainPresenter: IMainPresenter? = null
    var pagerPresenter: IPagerPresenter? = null
    private val handler: Handler = Handler()
    private val refresher = object : Runnable {
        override fun run() {
            //refresh regularly using a Handler
            fetchArticles()
            handler.postDelayed(this, REFRESH_RATE_MS)
        }
    }

    fun start() {
        if (isRunning) { //already running, only need to start once
            return
        }
        isRunning = true
        refresher.run()
    }

    private fun fetchArticles() {
        mainPresenter?.fetchArticles() //refreshes the database and main
        pagerPresenter?.fetchArticles() //just refreshes the shown list using the database
    }

    fun showLoadingDialog() {
        mainPresenter?.showLoadingDialog()
        pagerPresenter?.showLoadingDialog()
    }

    fun hideLoadingDialog() {
        mainPresenter?.hideLoadingDialog()
        pagerPresenter?.hideLoadingDialog()
    }
}