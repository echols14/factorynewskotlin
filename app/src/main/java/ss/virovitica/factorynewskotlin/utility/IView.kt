package ss.virovitica.factorynewskotlin.utility

import android.content.Context

interface IView {
    val context: Context
}